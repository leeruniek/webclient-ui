import expect from "expect";
import React from "react"
import Adapter from "enzyme-adapter-react-16"
import { shallow, configure } from "enzyme"
configure({ adapter: new Adapter() })

import { LUInput } from "./input"

test("LUInput pass properties to simple input element", () => {
  expect.assertions(10)

  const wrapper = shallow(
    <div>
      <LUInput
        type="text"
        name="Test name"
        defaultValue="Test defaultValue"
        isMultiline={false}
        isDisabled={false}
        isRequired={true}
        value="Test value"
        placeholder="Test placeholder"
        maxLength={10}
      />
    </div>
  )
  const component = wrapper.find(LUInput)
  const inputElement = component.dive().find("input")

  expect(inputElement.length).toBe(1)
  expect(!inputElement.props().hasOwnProperty("isMultiline")).toBeTruthy()
  expect(inputElement.prop("type")).toBe(component.prop("type"))
  expect(inputElement.prop("name")).toBe(component.prop("name"))
  expect(inputElement.prop("defaultValue")).toBe(component.prop("defaultValue"))
  expect(inputElement.prop("disabled")).toBe(component.prop("isDisabled"))
  expect(inputElement.prop("required")).toBe(component.prop("isRequired"))
  expect(inputElement.prop("value")).toBe(component.prop("value"))
  expect(inputElement.prop("placeholder")).toBe(component.prop("placeholder"))
  expect(inputElement.prop("maxLength")).toBe(component.prop("maxLength"))
})

test("LUInput render text area when 'isMultiline' prop is true", () => {
  expect.assertions(2)

  const wrapper = shallow(
    <div>
      <LUInput
        type="text"
        name="Test name"
        defaultValue="Test defaultValue"
        isMultiline={true}
        isDisabled={false}
        isRequired={true}
        value="Test value"
        placeholder="Test placeholder"
      />
    </div>
  )
  const component = wrapper.find(LUInput)

  expect(component.dive().find("input").length).toBe(0)
  expect(component.dive().find("textarea").length).toBe(1)
})

test("LUInput render label with required flag", () => {
  expect.assertions(2)

  const labelText = "Test label text"
  const wrapper = shallow(
    <div>
      <LUInput
        type="text"
        name="not required field"
        label={labelText}
        defaultValue="Test defaultValue"
        isMultiline={false}
        isDisabled={false}
        isRequired={false}
        value="Test value"
        placeholder="Test placeholder"
        hasBar={true}
      />
      <LUInput
        type="text"
        name="required field"
        label={labelText}
        defaultValue="Test defaultValue"
        isMultiline={false}
        isDisabled={false}
        isRequired={true}
        value="Test value"
        placeholder="Test placeholder"
        hasBar={true}
      />
    </div>
  )
  const requiredInput = wrapper.findWhere(el => el.prop("isRequired") === true)
  const notRequiredInput = wrapper.findWhere(
    el => el.prop("isRequired") === false
  )

  expect(requiredInput
    .dive()
    .find("label")
    .text()
    .trim()).toBe(`${labelText} *`)
  expect(notRequiredInput
    .dive()
    .find("label")
    .text()
    .trim()).toBe(labelText)
})
