import expect from "expect"
import React from "react"
import Adapter from "enzyme-adapter-react-16"
import { shallow, configure } from "enzyme"

import { LUTag } from "./tag"

test("LUTag renders when provided with a label", () => {
  expect.assertions(1)
  configure({ adapter: new Adapter() })

  const wrapper = shallow(<LUTag label="Tag label" />)

  expect(wrapper.text()).toEqual("Tag label")
})

test("LUTag renders an icon by default if type prop is 'info' or 'warning'", () => {
  expect.assertions(2)
  configure({ adapter: new Adapter() })

  const infoWrapper = shallow(<LUTag label="Info tag" type="info" />)
  const warningWrapper = shallow(<LUTag label="Warning tag" type="warning" />)

  expect(infoWrapper.find(<i className="fa fa-info" />)).toBeTruthy()
  expect(warningWrapper.find(<i className="fa fa-warning" />)).toBeTruthy()
})
