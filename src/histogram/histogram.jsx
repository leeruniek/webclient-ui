// @flow

import * as React from "react"
import { map, reduce } from "@leeruniek/functies"

import css from "./histogram.module.css"

type LUHistogramPropsType = {
  label: string,
  labelTotal?: string,
  data: Object,
  ticks: string[],
  hasTotal?: boolean,
  hasCount?: boolean,
  countRenderer?: Function,
}

export const LUHistogram = ({
  label,
  labelTotal = "Totaal",
  data,
  ticks,
  hasTotal = true,
  hasCount = false,
  countRenderer = (count = 0) => count,
}: LUHistogramPropsType): React.Node => {
  const total = reduce((acc, element) => acc + element, 0)(Object.values(data))

  return (
    <div className={css["histogram--table"]}>
      <div className={css["histogram--table__counter"]}>
        {hasTotal ? (
          <React.Fragment>
            <div className={css["histogram--table__label"]}>{labelTotal}</div>
            <div>{countRenderer(total)}</div>
          </React.Fragment>
        ) : null}
      </div>
      <div className={css["histogram--table__content"]}>
        <div className={css["histogram--table__label"]}>{label}</div>
        {map(value => {
          const percent = (data[value] / total) * 100

          return (
            <span key={value} className={css.histogram__item}>
              <strong>{value}: </strong>
              <span>{data[value] ? Math.round(percent) : "0"}%</span>
              {hasCount ? <span> - {countRenderer(data[value])}</span> : null}
            </span>
          )
        })(ticks)}
      </div>
    </div>
  )
}
