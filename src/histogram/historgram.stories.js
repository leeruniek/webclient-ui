import React from "react"
import { storiesOf } from "@storybook/react"
import { LUHistogram } from "./histogram"

const histogramProps = {
  label: "Verdeling",
  labelTotal: "Totaal aantal",
  data: {
    ">4": 1,
    "4-2": 6,
    "<2": 18,
  },
  hasCount: true,
  countRenderer: (amount = 0) => `${amount} leerlingen`,
  ticks: [">4", "4-2", "<2"],
}

storiesOf("LUHistogram", module).add("LUHistogram", () => (
  <LUHistogram {...histogramProps} />
))
