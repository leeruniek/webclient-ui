// @flow

import * as React from "react"
import DOMPurify from "dompurify"
import { replace, isEmpty, escapeRegExp } from "@leeruniek/functies"

import css from "./text-highlight.module.css"

type LUTextHighlightPropsType = {
  className?: string,
  value: string,
  query?: string,
  minQueryLength?: number,
  ignoreCase?: boolean,
}

const highlight = (query, ignoreCase) =>
  replace(
    new RegExp(`(${escapeRegExp(query)})`, ignoreCase ? "ig" : "g"),
    `<span class="${css.highlight}">$1</span>`
  )

export const LUTextHighlight = React.memo<LUTextHighlightPropsType>(
  ({
    className = "",
    value,
    query = "",
    minQueryLength = 1,
    ignoreCase = false,
  }) =>
    isEmpty(query) || query.length < minQueryLength || !value ? (
      value
    ) : (
      <span
        className={className}
        dangerouslySetInnerHTML={{
          __html: DOMPurify.sanitize(highlight(query, ignoreCase)(value), {
            ALLOWED_TAGS: ["span"],
          }),
        }}
      />
    )
)
