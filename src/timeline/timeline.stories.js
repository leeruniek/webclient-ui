import React from "react"
import { storiesOf } from "@storybook/react"
import { LUTimeline } from "./timeline"

const items = [
  {
    id: 1,
    title: "Basis",
    date: "2019-08-25T17:11:11.532701Z",
    user: "Joyce Bakker",
    icon: "arrows-h",
    schoolYear: "2019/2020",
  },
  {
    id: 2,
    title: "Basis",
    date: "2019-02-15T09:43:35.393863Z",
    icon: "arrows-h",
    user: "Juf Joyce",
    schoolYear: "20118/2019",
  },
  {
    id: 3,
    title: "Verrijkt",
    date: "2018-09-24T15:19:22.486715Z",
    user: "Leeruniek",
    icon: "arrows-h",
    schoolYear: "20118/2019",
  },
]

storiesOf("LUTimeline", module)
  .add("Simple timeline", () => (
    <LUTimeline
      items={items}
      renderChild={(): React.Node => <p>Any component can go here</p>}
    />
  ))
  .add("Timeline with dividers", () => (
    <LUTimeline
      items={items}
      divider="schoolYear"
      renderChild={(): React.Node => <p>Any component can go here</p>}
    />
  ))
