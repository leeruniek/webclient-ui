// @flow

import * as React from "react"
import cx from "classnames"
import { map, is, isEmpty } from "@leeruniek/functies"

import { LUTableCell } from "./table__cell"

import css from "./table.module.css"

type LUTableType = {|
  selectedId?: number | null,
  columns?: {
    [string]: {
      header: string,
      isSortable?: boolean,
      onCellRender: Function,
      headerClassName?: string,
    },
  },
  rows?: {
    id?: number,
    color?: string,
  }[],
  sortField?: string,
  sortDirection?: "DESC" | "ASC",
  onSort?: Function,
  onRowClick?: Function,
  customCss?: {
    table?: string,
    headerRow?: string,
    headerCell?: string,
    tableRow?: string,
    tableCell?: string,
  },
  isBiColor?: boolean,
  rowColors?: {
    even: string,
    odd: string,
  },
|}

export const LUTable = React.memo<LUTableType>(
  ({
    selectedId = null,
    columns = {},
    rows = [],
    sortField,
    sortDirection,
    onSort,
    onRowClick,
    customCss = {},
    isBiColor = false,
    rowColors = {},
  }: LUTableType): React.Node => {
    const handleSort = field => () => {
      // ASC -> DESC -> None -> repeat
      const step =
        field === sortField && sortDirection === "DESC"
          ? 3
          : sortDirection === "ASC"
          ? 2
          : 1

      onSort &&
        onSort({
          field: step === 3 ? null : field,
          direction: step === 3 ? null : step === 2 ? "DESC" : "ASC",
        })
    }

    const handleRowClick = item => () => {
      onRowClick && onRowClick(item)
    }

    const getRowColor = (rowId, rowColor) => {
      if (rowColor) {
        return { background: rowColor }
      }

      if (isBiColor && !isEmpty(rowColors)) {
        return rowId % 2
          ? { background: rowColors.even }
          : { background: rowColors.odd }
      }

      return null
    }

    const dataEntries = Object.entries(columns)

    return (
      <div
        className={cx(css.table, {
          [customCss.table || ""]: is(customCss.table),
        })}>
        <div
          className={cx(css.table__row, {
            [customCss.headerRow || ""]: is(customCss.headerRow),
          })}>
          {map(([columnKey, columnData], idx) => {
            const isFieldSorted = sortField === columnKey

            return (
              <LUTableCell
                key={`table-header-${idx}`}
                className={cx({
                  [customCss.headerCell || ""]: customCss.headerCell,
                  [columnData.headerClassName || ""]: columnData.headerClassName,
                  [css["table__head--sortable"]]: columnData.isSortable,
                })}
                isHeader={true}
                onClick={columnData.isSortable ? handleSort(columnKey) : null}>
                {columnData.header}
                {columnData.isSortable ? (
                  <i
                    className={cx(css["table__cell-icon"], "fa", {
                      "fa-sort": columnData.isSortable,
                      "fa-sort-up": isFieldSorted && sortDirection === "ASC",
                      "fa-sort-down": isFieldSorted && sortDirection === "DESC",
                    })}
                  />
                ) : null}
              </LUTableCell>
            )
          })(dataEntries)}
        </div>
        {map((row, idRow) => {
          return (
            <div
              key={`table-row-${idRow}`}
              style={getRowColor(idRow, row.color)}
              className={cx(css.table__row, {
                [customCss.tableRow || ""]: is(customCss.tableRow),
                [css["table__row--selected"]]: row.id === selectedId,
                [css["table__row--hoverable"]]: !!onRowClick,
              })}
              onClick={onRowClick ? handleRowClick(row) : null}>
              {map(([columnKey, columnData], idCell) => (
                <LUTableCell
                  key={`table-cell-${idRow}-${idCell}`}
                  className={cx({
                    [customCss.tableCell || ""]: is(customCss.tableCell),
                  })}>
                  {columnData.onCellRender
                    ? columnData.onCellRender(row)
                    : row[columnKey].header}
                </LUTableCell>
              ))(dataEntries)}
            </div>
          )
        })(rows)}
      </div>
    )
  }
)
