import expect from "expect"
import Adapter from "enzyme-adapter-react-16"
import { shallow, configure } from "enzyme"
import React from "react"
import css from "./tabs.module.css"

import { LUTabsItem, NavItem, BLINK_TIMEOUT } from "./tabs-item"

describe("LUTabsItem blinks and then stops blinking", () => {
  it("should not blink indfeinitiely if clicked twice", done => {
    jest.useFakeTimers()
    configure({ adapter: new Adapter() })
    const wrapper = shallow(<LUTabsItem isActive={true} />)

    expect(wrapper.find(NavItem).length).toEqual(1)
    expect(wrapper.find(NavItem).prop("className")).not.toContain(
      css["tab--blink"]
    )
    //click on it twice
    wrapper.find(NavItem).simulate("click")
    wrapper.find(NavItem).simulate("click")

    //check that it blinks
    expect(wrapper.find(NavItem).length).toEqual(1)
    expect(wrapper.find(NavItem).prop("className")).toContain(css["tab--blink"])

    //check that it stopped blinking after a period of time
    setTimeout(() => {
      expect(wrapper.find(NavItem).length).toEqual(1)
      expect(wrapper.find(NavItem).prop("className")).not.toContain(
        css["tab--blink"]
      )
      expect(wrapper.find(NavItem).prop("className")).toContain(
        css["tab tab--active"]
      )
      done()
    }, BLINK_TIMEOUT + 100)

    expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), BLINK_TIMEOUT)

    jest.runAllTimers()
  })
})
