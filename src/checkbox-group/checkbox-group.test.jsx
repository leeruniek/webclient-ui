import expect from "expect";
import React from "react"
import Adapter from "enzyme-adapter-react-16"
import { shallow, configure } from "enzyme"
configure({ adapter: new Adapter() })

import { LUCheckboxGroup } from "./checkbox-group"
import { LUCheckbox } from "../checkbox/checkbox"
import { LUCheckboxGroupPage } from "./checkbox-group.page"
import { LUCheckboxGroupHeader } from "../checkbox-group/checkbox-group__header"

test("Checkboxgroup contains 3 checkboxes", () => {
  expect.assertions(1)
  const wrapper = shallow(<LUCheckboxGroupPage />)
  const checkboxGroup = wrapper.find(LUCheckboxGroup)

  expect(checkboxGroup.find(LUCheckbox).length).toBe(3)
})

test("Checkbox header rendering with text", () => {
  expect.assertions(1)
  const header = shallow(<LUCheckboxGroupHeader label="Test example" />)

  expect(header.text()).toBe("Test example")
})

test("Checkboxgroup rendered with header with passed label", () => {
  expect.assertions(2)
  const eventHandler = () => "OK"
  const wrapper = shallow(
    <LUCheckboxGroup onChange={eventHandler} label="Test" selectedValues={[]}>
      <LUCheckbox label="1" name="1" />
    </LUCheckboxGroup>
  )

  expect(wrapper.find(LUCheckboxGroupHeader).length).not.toBe(0)
  const header = wrapper.find(LUCheckboxGroupHeader)

  expect(header.prop("label")).toBe("Test")
})

test(
  "Checkboxes have checked state related to selectedValues LUCheckboxGroup prop",
  () => {
    expect.assertions(2)
    const eventHandler = () => "OK"
    const testValues = ["1", "2"]
    const wrapper = shallow(
      <LUCheckboxGroup onChange={eventHandler} label="Test" selectedValues={[]}>
        <LUCheckbox label="1" name="1" />
        <LUCheckbox label="2" name="2" />
        <LUCheckbox label="3" name="3" />
      </LUCheckboxGroup>
    )

    expect(wrapper.find(LUCheckbox).length).toBe(wrapper.find({ isChecked: false }).length)
    wrapper.setProps({ selectedValues: testValues })
    expect(wrapper.find({ isChecked: true }).length).toBe(testValues.length)
  }
)

test("Checkboxgroup passsed onChange handler into children", () => {
  expect.assertions(1)
  const eventHandler = () => "OK"
  const wrapper = shallow(
    <LUCheckboxGroup onChange={eventHandler} selectedValues={[]}>
      <LUCheckbox label="checkboxOne" />
    </LUCheckboxGroup>
  )
  const checkbox = wrapper.find(LUCheckbox)

  expect(checkbox.props().onChange()).toBe("OK")
})
