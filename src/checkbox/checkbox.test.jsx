import expect from "expect";
import React from "react"
import Adapter from "enzyme-adapter-react-16"
import { shallow, configure } from "enzyme"
import TestRenderer from "react-test-renderer"
configure({ adapter: new Adapter() })

import { LUCheckbox } from "./checkbox"
import { LUCheckboxPage } from "./checkbox.page"

test("Checbox with label 'Enable'", () => {
  expect.assertions(2)
  const component = TestRenderer.create(<LUCheckbox label="Enable" />).toJSON()
  const label = component.children.find(el => el.type === "span")

  expect(label.type).toBe("span")
  expect(label.children).toEqual(["Enable"])
})

test("LUCheckbox isDisabled props passed from container state", () => {
  expect.assertions(2)
  const wrapper = shallow(<LUCheckboxPage />)

  wrapper.setState({ isDisabled: true })
  let checkboxComponent = wrapper.find({ label: "Test example" })

  expect(checkboxComponent.props().isDisabled).toBe(true)

  wrapper.setState({ isDisabled: false })
  checkboxComponent = wrapper.find({ label: "Test example" })
  expect(checkboxComponent.props().isDisabled).toBe(false)
})

test("LUCheckbox isChecked props passed from container state", () => {
  expect.assertions(2)
  const wrapper = shallow(<LUCheckboxPage />)

  wrapper.setState({ isChecked: true })
  let checkboxComponent = wrapper.find({ label: "Test example" })

  expect(checkboxComponent.props().isChecked).toBe(true)

  wrapper.setState({ isChecked: false })
  checkboxComponent = wrapper.find({ label: "Test example" })
  expect(checkboxComponent.props().isChecked).toBe(false)
})
