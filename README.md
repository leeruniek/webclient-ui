# webclient-ui

> Pure UI components

[![Coverage Status](https://coveralls.io/repos/github/Leeruniek/webclient-ui/badge.svg?branch=master)](https://coveralls.io/github/Leeruniek/webclient-ui?branch=master)

---

<!-- MarkdownTOC levels="1,2,3" autolink="true" indent="  " -->

- [Install](#install)
- [Use](#use)
- [Develop](#develop)
- [Changelog](#changelog)
  - [0.4.1 - November 29](#041---29-november)

<!-- /MarkdownTOC -->

## Docs

Check our [storybook](https://leeruniek.github.io/webclient-ui/) to see components that we use.

## Install

```bash
npm i eslint @leeruniek/webclient-ui --save-dev
```

## Use

```js
import { LUCheckbox } from "@leeruniek/webclient-ui"

render() {
  return <LUCheckbox label="UI component from separate repo"/>
}
```

## Develop

```bash
git clone git@github.com:leeruniek/webclient-ui.git && \
  cd webclient-ui && \
  npm run setup

# run tests (any `*.test.js`) once
npm test

# watch `src` folder for changes and run test automatically
npm run tdd
```

## Changelog

History of all changes in [CHANGELOG.md](CHANGELOG.md)

## [0.4.1] - 29 November 2019

### Improvements

- Allow `LURadioGroup`s to be laid out horizontally
